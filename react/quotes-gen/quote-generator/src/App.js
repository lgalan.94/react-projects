import './App.css';
import NavigationBar from './components/NavigationBar';
import Login from './auth/Login';
import Logout from './auth/Logout';
import Home from './frontend/Home';
import AboutWebsite from './frontend/About';
import GenerateQuote from './frontend/GenerateQuote';
import PageNotFound from './frontend/PageNotFound'

/*Admin Pages*/
import AdminHome from './admin/pages/AdminHome';
import AddQuotes from './admin/pages/AddQuotes';
import QuotesList from './admin/pages/QuotesList';

import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext.js';
import 'react-toastify/dist/ReactToastify.css';

function App() {

const [user, setUser] = useState({
  id: null,
  isAdmin: null
})

const unsetUser = () => {
  localStorage.clear();
}

useEffect(() => {

    if(localStorage.getItem('token')) {
        fetch(`${process.env.REACT_APP_API_URL}/user/user-details`, {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(result => result.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

}, [])

const Inaccessible = () => {
   return <Navigate to="/inaccessible" />;
 }

  return (
    <UserProvider value = {{ user, setUser, unsetUser }} >
      <BrowserRouter>
        <NavigationBar />
          <Routes>

            
              <Route path="/admin/dashboard" element={<AdminHome />} />
              <Route path="/admin/add-quotes" element={<AddQuotes />} />
              <Route path="/admin/quotes-list" element={<QuotesList />} />

              <Route path="/about" element={<AboutWebsite />} />
              <Route path="/generate" element={<GenerateQuote />} />
            
               
              <Route path="/" element={<Home />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />

              

              <Route path="/inaccessible" element={<PageNotFound/>} />
              <Route path="*" element={<Inaccessible />} />
          </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
