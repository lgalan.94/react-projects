import React from 'react';
import { Button } from 'react-bootstrap';

const QuotesTable = ({ quotes }) => {
let i = 0;

  return (
    <>
      {quotes.map(myquote => {
        i++;
        const { _id, quote, author, category, isActive } = myquote;

        return (
          <tr className="table-data" key={_id}>
            <td>{i}</td>
            <td>{quote}</td>
            <td>{author}</td>
            <td className="text-center">{category}</td>
            <td className="text-center">
              { isActive ? <span className="badge badge-active">Active</span> : <span className="badge badge-inActive">InActive</span> }
            </td>
            <td className="text-center">
              
            </td>
          </tr>
        );
      })}
    </>
  );
};

export default QuotesTable;