import { useState } from 'react';
import { Container, Form, Button, Row, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import '../css/adminStyles.css';

const API = `${process.env.REACT_APP_API_URL}/quotes/add-quotes`;

export default function AddQuotes() {
  const navigate = useNavigate();
  const [quotes, setQuotes] = useState([{ quote: '', category: '', author: '' }]);

  const handleAddForm = () => {
    setQuotes([...quotes, { quote: '', category: '', author: '' }]);
  };

  const handleInputChange = (event, index) => {
    const { name, value } = event.target;
    const list = [...quotes];
    list[index][name] = value;
    setQuotes(list);
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    fetch(API, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(quotes)
    })
      .then(result => result.json())
      .then(data => {
        if (data === true) {
          alert(`added`);
          navigate('/admin/quotes-list');
        } else {
          alert('Error adding quotes!');
        }
      });
  };

  const handleRemoveForm = () => {
    const list = [...quotes];
    list.pop();
    setQuotes(list);
  };

  return (
    <>
      <div className="text-center">
        <Button className="btnAddForm me-2" variant="primary" onClick={handleAddForm}>Add Form</Button>
        <Button className="btnRemoveForm" variant="secondary" onClick={handleRemoveForm}>Remove Form</Button>
      </div>

      <Container className="mt-3 add-quotes-container">
        <Form onSubmit={handleFormSubmit}>
          {quotes.map((quote, index) => (
            <Row key={index}>
              <Col className="col-md-6">
                <Form.Group className="mb-3" controlId={`quote-${index}`}>
                  <Form.Label>Quote</Form.Label>
                  <Form.Control as="textarea" rows={1} name="quote" value={quote.quote} onChange={(event) => handleInputChange(event, index)} />
                </Form.Group>
              </Col>
              <Col className="col-md-3">
                <Form.Group className="mb-3" controlId={`author-${index}`}>
                  <Form.Label>Author</Form.Label>
                  <Form.Control as="textarea" rows={1} name="author" value={quote.author} onChange={(event) => handleInputChange(event, index)} />
                </Form.Group>
              </Col>
              <Col className="col-md-3">
                <Form.Group className="mb-3" controlId={`category-${index}`}>
                  <Form.Label>Category</Form.Label>
                  <Form.Select name="category" value={quote.category} onChange={(event) => handleInputChange(event, index)}>
                    <option value="All">All</option>
                    <option value="Work">Work</option>
                    <option value="Life">Life</option>
                    <option value="Success">Success</option>
                    <option value="Fear">Fear</option>
                    <option value="Effort">Effort</option>
                    <option value="Motivation">Motivation</option>
                    <option value="Self-improvement">Self-improvement</option>
                    <option value="Love">Love</option>
                  </Form.Select>
                </Form.Group>
              </Col>
            </Row>
          ))}
          <div className="text-center mb-3">
            <Button className="btnAddForm mt-5" variant="primary" type="submit">Add</Button>
          </div>
        </Form>
      </Container>
    </>
  );
}