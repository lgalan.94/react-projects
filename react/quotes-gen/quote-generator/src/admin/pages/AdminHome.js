import { useEffect, useContext } from 'react';
import UserContext from '../../UserContext';

export default function AdminHome(){

const { setUser } = useContext(UserContext)

useEffect(() => {

    if(localStorage.getItem('token')) {
        fetch(`${process.env.REACT_APP_API_URL}/user/user-details`, {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(result => result.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

}, [])

	return (
		<>	
			<h1 className = "text-center mt-5" >This is ADMIN HOME!</h1>
		</>

	)
}