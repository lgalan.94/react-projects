import React, { useEffect, useState } from 'react';
import { Table, Container, Form } from 'react-bootstrap';
import QuotesTable from '../components/QuotesTable';
import '../css/adminStyles.css';

const QuotesList = () => {
  const [quotes, setQuotes] = useState([]);
  const [filteredQuotes, setFilteredQuotes] = useState([]);
  const [filterCategory, setFilterCategory] = useState('');

  const fetchQuotes = () => {
    fetch(`${process.env.REACT_APP_API_URL}/quotes/all-quotes`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-type': 'application/json'
      }
    })
      .then(result => result.json())
      .then(data => {
        console.log(data);
        if (data.length > 0) {
          setQuotes(data);
          setFilteredQuotes(data);
        }
      });
  };

  useEffect(() => {
    fetchQuotes();
  }, []);

  const handleFilter = e => {
    setFilterCategory(e.target.value);
  };

  useEffect(() => {
    if (filterCategory === 'All') {
      setFilteredQuotes(quotes);
    } else if (filterCategory) {
      const filtered = quotes.filter(quote => quote.category === filterCategory);
      setFilteredQuotes(filtered);
    }
  }, [filterCategory, quotes]);

  const categories = ['All', 'Work', 'Life', 'Success', 'Fear', 'Effort', 'Motivation', 'Self-improvement', 'Love'];

  return (
    <Container fluid className="QuotesList">
      <Form className="filterCategory">
        <Form.Control
          as="select"
          className="mr-sm-2"
          value={filterCategory}
          onChange={handleFilter}
        >
          {categories.map(category => (
            <option key={category}>{category}</option>
          ))}
        </Form.Control>
      </Form>
      <div className="table-scrollable">
        <Table className="table" striped bordered hover size="sm">
          <thead className="table-header table-primary">
            <tr>
              <th col-width="5%">#</th>
              <th col-width="50%">Quote</th>
              <th col-width="50%">Author</th>
              <th col-width="15%" className="text-center" >Category</th>
              <th col-width="10%">Status</th>
              <th col-width="20%" className="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            <QuotesTable quotes={filteredQuotes} />
          </tbody>
        </Table>
      </div>
    </Container>
  );
};

export default QuotesList;