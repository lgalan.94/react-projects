import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import PageProgress from 'react-page-progress';

import UserContext from '../UserContext.js';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isDisabled, setIsDisabled] = useState(true);

  const { setUser } = useContext(UserContext);

  const navigate = useNavigate();

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/user/user-details`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(result => result.json())
      .then(data => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });

        if (data.isAdmin) {
          setTimeout(() => navigate('/admin/dashboard'), 1000);
          // navigate('/admin/dashboard')
        } else {
          setTimeout(() => navigate('/'), 1000);
        }
      });
  };

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [email, password]); //dependencies

  const handleLogin = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(result => result.json())
      .then(data => {
        if (data === false) {
          toast.error("Invalid email or password!");
        } else {
          toast.success("Login Successful!");
          localStorage.setItem('token', data.auth);
          retrieveUserDetails(data.auth);
        }
      });
  };

  return (
    <>
    <PageProgress
      element="#content"
      height="4"
      color="green"
      style={{
        backgroundColor: "white",
        borderRadius: "5px",
      }}
      visible={true}
    />
    <Container className="mt-5 loginContainer">
      <Row className="justify-content-center">
        <Col className="col-md-5 col-sm-6">
          <h1 className="text-center">Login</h1>
          <Form onSubmit={handleLogin}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                value={email}
                onChange={event => setEmail(event.target.value)}
                type="email"
                placeholder="Enter email"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                value={password}
                onChange={event => setPassword(event.target.value)}
                type="password"
                placeholder="Password"
              />
            </Form.Group>

            <p>
              No Account Yet? <Link as={Link} to="/register">Sign-Up Here</Link>
            </p>
            <div className="text-center">
              <Button
                className="loginBtn"
                disabled={isDisabled}
                variant="dark"
                type="submit"
              >
                Login
              </Button>
            </div>
          </Form>
          <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
          />
          {/* Same as */}
          <ToastContainer />
        </Col>
      </Row>
    </Container>
    </>
  );
}