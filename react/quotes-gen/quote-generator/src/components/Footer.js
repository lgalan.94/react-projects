import React from "react";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container">
        
        <div className="copyright">
          &copy; 2023 Created by LMGJ ── Newbie Developer
        </div>
      </div>
    </footer>
  );
};

export default Footer;
