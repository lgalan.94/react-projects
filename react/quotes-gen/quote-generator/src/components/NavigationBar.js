import { Container, Navbar, Nav } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { useContext, useEffect, useState } from 'react';
 
export default function NavigationBar() {

const { user } = useContext(UserContext);

const [email, setEmail] = useState('')

useEffect(() => {
	fetch(`${process.env.REACT_APP_API_URL}/user/user-details`, {
	  method: 'GET',
	  headers: {
	    Authorization: `Bearer ${localStorage.getItem('token')}`
	  }
	})
	.then(result => result.json())
	.then(data => {
	   	setEmail(data.email)
	})
})

	return (
		<>

				{
					user.isAdmin ? 

							<Navbar sticky="top" bg="dark" data-bs-theme="dark" expand="lg" className="bg-body-tertiary">
						      <Container>
						      <Navbar.Brand as = {Link} to = '/admin/dashboard' >
					              <img
					                src="../brainbytes-icon.png"
					                width="50"
					                height="50"
					                className="rounded d-inline-block align-top"
					                alt="brain bytes logo"
					              />
						        </Navbar.Brand>
						        <Navbar.Brand as = {Link} to = '/admin/dashboard' >Brain Bytes - ADMIN</Navbar.Brand>
						        <Navbar.Toggle aria-controls="basic-navbar-nav" />
						        <Navbar.Collapse id="basic-navbar-nav">
						          <Nav className="ms-auto">
						            <Nav.Link as = {NavLink} to='/admin/dashboard' >Dashboard</Nav.Link>
						            <Nav.Link as = {NavLink} to='/admin/quotes-list' >Quotes List</Nav.Link>
			                        <Nav.Link as = {NavLink} to='/admin/add-quotes' >Add Quotes</Nav.Link>
			                        <Nav.Link as = {NavLink} to='/admin/list-of-users' >Users</Nav.Link>
						            <Nav.Link as = {NavLink} to='/logout' >Logout</Nav.Link>  
						          </Nav>
						        </Navbar.Collapse>
						      </Container>
							</Navbar>

							:

							<Navbar sticky="top" expand="lg" className="bg-body-tertiary">
						      <Container>
						      <Navbar.Brand as = {Link} to = '/'>
					              <img
					                src="brainbytes-icon.png"
					                width="50"
					                height="50"
					                className="rounded d-inline-block align-top"
					                alt="brain bytes logo"
					              />
						        </Navbar.Brand>
						        <Navbar.Brand as = {Link} to = '/'>Brain Bytes</Navbar.Brand>
						        <Navbar.Toggle aria-controls="basic-navbar-nav" />
						        <Navbar.Collapse id="basic-navbar-nav">
						          <Nav className="ms-auto">
						            <Nav.Link as = {NavLink} to='/' >Home</Nav.Link>
						            <Nav.Link as = {NavLink} to='/about' >About </Nav.Link>
						            <Nav.Link as = {NavLink} to='/generate' >Generate </Nav.Link>

						            {
						            	user.id !== null ? 
						            	(
						            		<>
								            	<Nav.Link as = {NavLink} to='/logout' >Logout</Nav.Link>
								            	<Nav.Link >{email}</Nav.Link>
							            	</>
							            )
						            	:
						            	(
							            	<>
								            	{/*<Nav.Link as = {NavLink} to='/login' >Login</Nav.Link>*/}
								            	{/*<Nav.Link as = {NavLink} to='/register' >Sign-Up</Nav.Link>*/}
							            	</>
						            	)
						            }

					        			
						            
						          </Nav>
						        </Navbar.Collapse>
						      </Container>
							</Navbar>

				}

				{/*<Container className="text-end">
					<small style={{ color: 'deepskyblue' }}  >v.1.0</small>
				</Container>*/}

		</>
	);
}