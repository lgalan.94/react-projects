import { useState } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { AiOutlineCopy } from "react-icons/ai";

export default function QuotesCard(props) {
  const [isCopied, setIsCopied] = useState(false);

  const { quote, category, author } = props.quoteProp;

  const handleCopy = () => {
   navigator.clipboard.writeText(quote);
   setIsCopied(true);
   setTimeout(() => {
     setIsCopied(false);
   }, 3000);
 };

 return (
  <Container>
      <Row className="mt-5">
          <Col>
              <Card className="QuotesCard">
                  <Card.Header className="text-center cardHeader">
                      <small className="text-muted">── {category} ──</small>
                  </Card.Header>

                  <Card.Body className="cardBody d-flex">
                        <Card.Subtitle className="mb-3 quote">“{quote}”</Card.Subtitle> 
                  </Card.Body>

                  <Card.Footer className="cardFooter text-muted d-flex justify-content-between">
                      <small title="Source Title" className="author"><em> ── {author} </em></small>
                      <Button onClick={handleCopy} className="copy-button">
                      {isCopied ? 'Copied!' : < AiOutlineCopy size={20} />}
                      </Button>
                  </Card.Footer>
              </Card>
          </Col>
      </Row>
  </Container>
  )

}