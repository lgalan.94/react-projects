import React from 'react';
import { Form } from 'react-bootstrap';

export default function SideToggleNav({ selectedCategory, handleFilterChange }) {
  return (
    <div>
      <Form.Select className="filter-select" value={selectedCategory} onChange={handleFilterChange}>
        <option value="All">All</option>
        <option value="Work">Work</option>
        <option value="Life">Life</option>
        <option value="Success">Success</option>
        <option value="Fear">Fear</option>
        <option value="Effort">Effort</option>
        <option value="Motivation">Motivation</option>
        <option value="Self-improvement">Self-improvement</option>
        <option value="Love">Love</option>
      </Form.Select>
    </div>
  );
}