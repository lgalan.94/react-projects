import { Container } from 'react-bootstrap';
import Footer from '../components/Footer'
import './frontend.css';

export default function AboutWebsite() {
	return (
		<>
		<Container fluid className = "AboutWebsiteContainer" >
			<h1>About</h1>
			<h5> Welcome to my website! I am passionate about creating engaging and entertaining content across various media platforms. Here, you will find a wide range of content, including quotes, riddles, jokes, and much more. </h5>

			<h6 className="mt-4">	<em> Quotes: </em> Get inspired and motivated with a collection of carefully curated quotes. Whether you're looking for words of wisdom, encouragement, or a fresh perspective, you'll find a diverse selection of quotes to uplift and empower you. </h6>

			<h6 className="mt-3">	<em> Riddles: </em> Challenge your mind and have fun with a collection of intriguing riddles. Test your problem-solving skills and enjoy the thrill of unraveling complex puzzles. From clever wordplay to logical conundrums, there's a riddle for everyone to enjoy. </h6>

			<h6 className="mt-3">	<em> Jokes: </em> Laughter is the best medicine, and I've got you covered with a collection of hilarious jokes. From puns to one-liners, these jokes are sure to bring a smile to your face and brighten your day. Share them with friends and family for a good laugh together. </h6>

			<h6 className="mt-3">	In addition to quotes, riddles, and jokes, I also offer a variety of other engaging content. From interesting facts to inspiring stories, there's always something new and exciting waiting for you to explore. </h6>

			<h6 className="mt-3">	I'm dedicated to providing you with high-quality and entertaining content that will keep you engaged and entertained. So, sit back, relax, and enjoy the diverse range of content available on my website. Thank you for visiting, and I hope you have a wonderful time exploring all that I have to offer! </h6>
		</Container>
		<Footer />
		</>
	)
}