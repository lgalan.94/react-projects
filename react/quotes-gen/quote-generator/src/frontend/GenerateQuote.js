
import React, { useState } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import QuotesCard from '../components/QuotesCard';
import Footer from '../components/Footer';
import { ToastContainer, toast } from 'react-toastify';

export default function GenerateQuote() {
  const [quote, setQuote] = useState([]);
  const [category, setCategory] = useState('');

  const fetchQuote = () => {
    let options = {
      method: 'GET',
      headers: { 'x-api-key': `sj17/POIHaDMxt0LmzGgPQ==Pnst9Ma4grIYKdN0` }
    };

    let url = `https://api.api-ninjas.com/v1/quotes?category=${encodeURIComponent(category)}`;
    fetch(url, options)
      .then(response => {
        if (!response.ok) {
          throw new Error('Request failed:', response.status);
        }
        return response.json();
      })
      .then(data => {
        if (data.length === 0) {
          toast.error("No results found! Try a different keyword.");
        } else {
          setQuote(
            data.map(quote => (
              <QuotesCard quoteProp={quote} />
            ))
          );
        }
      })
      .catch(error => {
        console.error('Request failed:', error);
      });
  };

  const handleCategoryChange = (event) => {
    setCategory(event.target.value);
  };

  const generateQuote = () => {
    fetchQuote();
    window.history.pushState(null, '', `?category=${encodeURIComponent(category)}`);
  };

  return (
    <>
      <Container className="generateQuoteContainer">
        <h1 className="mt-3 mb-2">Generate a Quote</h1>
        <Row className="justify-content-center">
          <Col>
            <div className="d-flex justify-content-center">
              <input
                className="search-bar"
                type="text"
                placeholder="Type category to generate a quote ..."
                value={category}
                onChange={handleCategoryChange}
              />
            </div>
          </Col>
        </Row>
        <div className="text-center mb-4">
          <Button onClick={generateQuote}>Generate</Button>
        </div>

        <Row className="my-2">
          <Col className="generated">
            {quote.length === 0 ? <p className="text-center text-white">Add category keyword to generate</p> : quote}
          </Col>
        </Row>

        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
        />
        {/* Same as */}
        <ToastContainer />

      </Container>

      <Container fluid className="choices">
        <h6 className="text-center pt-3">Category keywords you can try</h6>
        <hr />
        <Row>
          <Col className="col-md-2">
            <ul>
              <li>age</li>
              <li>alone</li>
              <li>amazing</li>
              <li>anger</li>
              <li>best</li>
              <li>hope</li>
            </ul>
          </Col>
          <Col className="col-md-2">
            <ul>
              <li>courage</li>
              <li>dreams</li>
              <li>experience</li>
              <li>failure</li>
              <li>faith</li>
              <li>good</li>
            </ul>
          </Col>
          <Col className="col-md-2">
            <ul>
              <li>family</li>
              <li>fear</li>
              <li>forgiveness</li>
              <li>freedom</li>
              <li>friendship</li>
              <li>home</li>
            </ul>
          </Col>
          <Col className="col-md-2">
            <ul>
              <li>god</li>
              <li>graduation</li>
              <li>happiness</li>
              <li>imagination</li>
              <li>inspirational</li>
              <li>knowledge</li>
            </ul>
          </Col>
          <Col className="col-md-2">
            <ul>
              <li>learning</li>
              <li>life</li>
              <li>love</li>
              <li>marriage</li>
              <li>mom</li>
              <li>leadership</li>
            </ul>
          </Col>
          <Col className="col-md-2">
            <ul>
              <li>money</li>
              <li>success</li>
              <li>jealousy</li>
              <li>intelligence</li>
              <li>great</li>
              <li>men</li>
            </ul>
          </Col>
        </Row>
      </Container>
      <Footer />
    </>
  );
}