import Quotes from './Quotes';
import Footer from '../components/Footer'

export default function Home(){
	return (
		<>
			<Quotes />
			<Footer />
		</>
	);
}