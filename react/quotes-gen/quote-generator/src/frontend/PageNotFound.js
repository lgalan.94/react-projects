import {Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function PageNotFound(){
	return(
		<>     

			<Container className = "text-center mt-5">

				<h1>Page Not Found</h1>

				<img style={{ width: '25%' }} src = "https://img.freepik.com/free-vector/page-found-concept-illustration_114360-1869.jpg?w=2000" />

				<p>Go back to the <Link as={Link} to='/'>homepage</Link></p>

			</Container>

		</>	

		)
}