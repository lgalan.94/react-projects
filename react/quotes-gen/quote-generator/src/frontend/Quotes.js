import { useState, useEffect } from 'react';
import { Container, Row, Col, Form } from 'react-bootstrap';
import QuotesCard from '../components/QuotesCard';

const API = `${process.env.REACT_APP_API_URL}/quotes/active-quotes`;

export default function Quotes() {
  const [quotes, setQuotes] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [selectedCategory, setSelectedCategory] = useState('All');

  useEffect(() => {
    fetch(API)
      .then(res => res.json())
      .then(data => {
        setQuotes(data);
      })
      .catch(err => {
        console.log(`error ${err}`);
      });
  }, []);

  const renderQuotesRows = () => {
    const filteredQuotes = quotes.filter(quote => {
      if (selectedCategory === 'All') {
        return quote.quote.toLowerCase().includes(searchTerm.toLowerCase());
      } else {
        return quote.category === selectedCategory && quote.quote.toLowerCase().includes(searchTerm.toLowerCase());
      }
    });

    if (filteredQuotes.length === 0) {
      return <div className="text-center noMatch">No matches! Try different keyword!</div>;
    }

    const rows = [];
    for (let i = 0; i < filteredQuotes.length; i += 3) {
      rows.push(
        <Row key={i} className="">
          {i < filteredQuotes.length && (
            <>
              <Col sm={12} md={12} lg={4}>
                <QuotesCard quoteProp={filteredQuotes[i]} />
              </Col>
              {i + 1 < filteredQuotes.length && (
                <Col sm={12} md={12} lg={4}>
                  <QuotesCard quoteProp={filteredQuotes[i + 1]} />
                </Col>
              )}
              {i + 2 < filteredQuotes.length && (
                <Col sm={12} md={12} lg={4}>
                  <QuotesCard quoteProp={filteredQuotes[i + 2]} />
                </Col>
              )}
            </>
          )}
        </Row>
      );
    }
    return rows;
  };

  const handleSearchChange = event => {
    setSearchTerm(event.target.value);
  };

  const handleFilterChange = event => {
    setSelectedCategory(event.target.value);
  };

  return (
    <>
      <Container className="quotesContainer">
        <Row>
          <Col className="col-md-2 col-12">
            <div>
              <Form.Select className="filter-select" value={selectedCategory} onChange={handleFilterChange}>
                <option value="All">All</option>
                <option value="Work">Work</option>
                <option value="Life">Life</option>
                <option value="Success">Success</option>
                <option value="Fear">Fear</option>
                <option value="Effort">Effort</option>
                <option value="Motivation">Motivation</option>
                <option value="Self-improvement">Self-improvement</option>
                <option value="Love">Love</option>
              </Form.Select>
            </div>
          </Col>
          <Col className="col-12 col-md-4 ">
            <div className="search">
              <input type="text" placeholder="Type a keyword to filter ..." value={searchTerm} onChange={handleSearchChange} />
            </div>
          </Col>
        </Row>
        {renderQuotesRows()}
      </Container>
    </>
  );
}