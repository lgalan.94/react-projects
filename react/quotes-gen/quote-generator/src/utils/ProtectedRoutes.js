import { Outlet, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import { useContext, useEffect, useState } from 'react';

export default function PrivateRoutes(){

const [email, setEmail] = useState('')

useEffect(() => {
	fetch(`${process.env.REACT_APP_API_URL}/user/user-details`, {
	  method: 'GET',
	  headers: {
	    Authorization: `Bearer ${localStorage.getItem('token')}`
	  }
	})
	.then(result => result.json())
	.then(data => {
	   	setEmail(data.email)
	})
})



const { user } = useContext(UserContext);

	return (
		<>
		{
			user.id !== null ? <Outlet /> : <Navigate to = '/login' />
		}
		</>
	);
}